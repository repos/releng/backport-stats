# Backport Stats

Gathers deployment log entries from the [server admin log](https://sal.toolforge.org/production) and parses them into a database...using a lot of regex.

## Usage

    ./gather-deploys-from-elastic.sh 2022-01-01 2023-01-01
    ./parse-deploys.py deploys-2022-01-01-2023-01-01.json deploys.db

This produces `deploys.db` with the following schema:

    id INTEGER PRIMARY KEY,
    type                       # One of 'sync_file', 'sync_world', 'backport', 'scap3'
    timestamp                  # Time of the sal entry
    duration                   # Duration in seconds
    user                       # User who ran the command
    host                       # Host they ran the command from
    message                    # Full message
    patchset                   # List: gerrit patchsets
    task                       # List: phabricator tasks
    sync_path                  # sync_file only: path sync'd
    scap3_repo                 # scap3 only: repo
    scap3_revision             # scap3 only: revision of repo (sha1)
    scap3_group                # scap3 only: server group
