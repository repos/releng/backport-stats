#!/usr/bin/env python
import argparse
import json
import re
import sqlite3

def make_deploy(deploy):
    deploy_message = deploy['message']
    if isinstance(deploy_message, list):
        deploy_message = deploy_message[-1]
    timestamp = deploy['@timestamp']
    nick = deploy['nick']
    if not nick:
        return None
    sync_file_regex = re.compile(
        r'^Synchronized (?P<path>[^:]+): (?P<deploy_message>.*)$')
    backport_regex = re.compile(
        r'^Finished scap: Backport for (?P<deploy_message>.*)$')
    sync_world_regex = re.compile(r'^Finished scap: (?P<deploy_message>.*)')
    scap3_regex = re.compile(''.join([
        r'^Finished deploy ',
        r'\[(?P<repo>[^@]+)@(?P<revision>[a-f0-9]+)\]\s?',
        r'(?P<group>\([\w_-]+\))?: ',
        r'(?P<deploy_message>.*)$',
    ]))

    match = None
    match = sync_file_regex.match(deploy_message)
    if match:
        return SyncFileDeploy(
            timestamp,
            Comment(match.group('deploy_message'), nick),
            match.group('path')
        )
    match = backport_regex.match(deploy_message)
    if match:
        return BackportDeploy(
            timestamp,
            Comment(match.group('deploy_message'), nick),
        )
    match = sync_world_regex.match(deploy_message)
    if match:
        return SyncWorldDeploy(
            timestamp,
            Comment(match.group('deploy_message'), nick),
        )
    match = scap3_regex.match(deploy_message)
    if match:
        return Scap3Deploy(
            timestamp,
            Comment(match.group('deploy_message'), nick),
            match.group('repo'),
            match.group('revision')
        )
    return None

def parse_duration(duration):
    """
    Parse a duration string into seconds.
    string like 1h 30m 10s
    """
    if not duration:
        return None
    duration = duration.strip()
    if not duration:
        return None
    seconds = 0
    for part in duration.split():
        if part[-1] == 's':
            seconds += int(part[:-1])
        elif part[-1] == 'm':
            seconds += int(part[:-1]) * 60
        elif part[-1] == 'h':
            seconds += int(part[:-1]) * 60 * 60
        else:
            raise ValueError('Unknown duration part: %s' % part)
    return seconds

class Comment:
    def __init__(self, message, nick):
        self.message = message
        self.nick = nick
        self.gerrit_regex = re.compile(r'\[\[gerrit:(?P<change_id>\d+)\|.*?\]\]')
        self.phab_regex = re.compile(r'\b(?P<phab>T\d+)\b')
        self.duration_regex = re.compile(r'\(duration: (?P<duration>[\dhms\s]+)\)')
        self.nick_regex = re.compile(r'^(?P<user>[^@]+)@(?P<host>.*)$')
        default = {'run': False, 'value': None}
        self._host = default.copy()
        self._user = default.copy()
        self._gerrit = default.copy()
        self._phab = default.copy()
        self._duration = default.copy()

    @property
    def gerrit(self):
        if not self._gerrit['run']:
            self._gerrit['run'] = True
            self._gerrit['value'] = [
                int(g) for g in self.gerrit_regex.findall(self.message)]
        return self._gerrit['value']

    @property
    def phab(self):
        if not self._phab['run']:
            self._phab['run'] = True
            self._phab['value'] = self.phab_regex.findall(self.message)
        return self._phab['value']

    @property
    def duration(self):
        if not self._duration['run']:
            duration = self.duration_regex.search(self.message)
            if duration:
                self._duration['value'] = parse_duration(duration.group('duration'))
            else:
                self._duration['value'] = None
            self._duration['run'] = True
        return self._duration['value']

    @property
    def user(self):
        if not self._user['run']:
            user, host = self._get_user_host()
            self._user['run'] = True
            self._user['value'] = user
            self._host['run'] = True
            self._host['value'] = host
        return self._user['value']

    @property
    def host(self):
        if not self._host['run']:
            user, host = self._get_user_host()
            self._host['run'] = True
            self._host['value'] = host
            self._user['run'] = True
            self._user['value'] = user
        return self._host['value']

    def _get_user_host(self):
        match = self.nick_regex.search(self.nick)
        if match:
            return (match.group('user'), match.group('host'))
        return (self.nick, None)


class Deploy:
    def __init__(self, timestamp, deploy_message):
        self.timestamp = timestamp
        self.deploy_message = deploy_message
        self.duration = self.deploy_message.duration
        self.user = self.deploy_message.user
        self.host = self.deploy_message.host
        self.message = self.deploy_message.message
        self.gerrit = json.dumps(self.deploy_message.gerrit)
        self.phab = json.dumps(self.deploy_message.phab)
        self.type = None
        self.path = None
        self.repo = None
        self.revision = None
        self.group = None


class SyncFileDeploy(Deploy):
    def __init__(self, timestamp, deploy_message, path):
        super().__init__(timestamp, deploy_message)
        self.path = path
        self.type = 'sync_file'


class BackportDeploy(Deploy):
    def __init__(self, timestamp, deploy_message):
        super().__init__(timestamp, deploy_message)
        self.type = 'backport'


class SyncWorldDeploy(Deploy):
    def __init__(self, timestamp, deploy_message):
        super().__init__(timestamp, deploy_message)
        self.type = 'sync_world'


class Scap3Deploy(Deploy):
    def __init__(self, timestamp, deploy_message, repo, revision, group=None):
        super().__init__(timestamp, deploy_message)
        self.repo = repo
        self.revision = revision
        self.group = group
        self.type = 'scap3'


def iter_deploys(deploy_logs):
    for deploy_log in deploy_logs:
        deploys = json.loads(deploy_log)['hits']['hits']
        for deployment in deploys:
            deployment = deployment['_source']
            deploy_message = deployment['message']
            deploy = make_deploy(deployment)
            if deploy:
                yield deploy


def make_db(db_path):
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute('''
    CREATE TABLE IF NOT EXISTS deploys (
        id INTEGER PRIMARY KEY,
        type TEXT not null,
        timestamp TEXT not null,
        duration TEXT not null,
        user TEXT not null,
        host TEXT,
        message TEXT not null,
        patchset TEXT,
        task TEXT,
        sync_path TEXT,
        scap3_repo TEXT,
        scap3_revision TEXT,
        scap3_group TEXT
    )
    ''')
    conn.commit()
    return c, conn


def insert_deployment(c, deploy):
    try:
        c.execute('''
        INSERT INTO deploys (
            type,
            timestamp,
            duration,
            user,
            host,
            message,
            patchset,
            task,
            sync_path,
            scap3_repo,
            scap3_revision,
            scap3_group
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ''', (
            deploy.type,
            deploy.timestamp,
            deploy.duration,
            deploy.user,
            deploy.host,
            deploy.message,
            deploy.gerrit,
            deploy.phab,
            deploy.path,
            deploy.repo,
            deploy.revision,
            deploy.group
        ))
    except Exception as e:
        print(e)
        import pdb; pdb.set_trace()
        raise e


def parse_args():
    parser = argparse.ArgumentParser(description='Parse deploy logs')
    parser.add_argument('json', help='input json file')
    parser.add_argument('db_path', help='path to sqlite db')

    return parser.parse_args()


def main():
    args = parse_args()
    with open(args.json, 'r') as f:
        deploy_logs = f.read().splitlines()
    print('Found %d deploy logs' % len(deploy_logs))

    c, conn = make_db(args.db_path)

    c.execute('BEGIN TRANSACTION')
    for i, deploy in enumerate(iter_deploys(deploy_logs)):
        insert_deployment(c, deploy)
        if i % 100 == 0:
            print('Committing %d' % i)
            conn.commit()
            c.execute('BEGIN TRANSACTION')
    conn.commit()
    conn.close()

if __name__ == '__main__':
    main()
