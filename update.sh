#!/usr/bin/env bash

set -euox pipefail

start_date="$(echo 'select timestamp from deploys order by timestamp desc limit 1;' | sqlite3 data/deploys.db)"
end_date="$(date --utc +'%Y-%m-%dT%H:%M:%SZ')"
bash gather.sh "$start_date" "$end_date" "data/deploys-$start_date-$end_date.json"
python3 parse-deploys.py "data/deploys-$start_date-$end_date.json" "data/deploys-$start_date-$end_date.db"
bash merge-dbs.sh "data/deploys-$start_date-$end_date.db" data/deploys.db
