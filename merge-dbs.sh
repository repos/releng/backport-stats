#!/usr/bin/env bash

set -euo pipefail

# Merge all the databases into one
# Usage: ./merge-dbs.sh <db1> <db2> <db3> ... <dbN> <merged-db>
# Example: ./merge-dbs.sh db1.db db2.db db3.db merged.db
# Note: The merged database will be overwritten if it already exists

INPUT_DBS=("${@:1:$#-1}")
MERGED_DB="${@: -1}"

echo "Merging ${#INPUT_DBS[@]} databases into ${MERGED_DB}..."
sqlite3 -batch "${MERGED_DB}" <<EOF
CREATE TABLE IF NOT EXISTS deploys (
    id INTEGER PRIMARY KEY,
    type TEXT not null,
    timestamp TEXT not null,
    duration TEXT not null,
    user TEXT not null,
    host TEXT,
    message TEXT not null,
    patchset TEXT,
    task TEXT,
    sync_path TEXT,
    scap3_repo TEXT,
    scap3_revision TEXT,
    scap3_group TEXT
);
EOF

for db in "${INPUT_DBS[@]}"; do
    echo "Merging ${db}..."
    sqlite3 -batch "${MERGED_DB}" <<EOF
ATTACH '${db}' AS toMerge;
INSERT INTO deploys (
    type,
    timestamp,
    duration,
    user,
    host,
    message,
    patchset,
    task,
    sync_path,
    scap3_repo,
    scap3_revision,
    scap3_group
) SELECT
    type,
    timestamp,
    duration,
    user,
    host,
    message,
    patchset,
    task,
    sync_path,
    scap3_repo,
    scap3_revision,
    scap3_group
FROM toMerge.deploys;
DETACH toMerge;
EOF
done
