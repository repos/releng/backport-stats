#!/usr/bin/env bash

START_DATE="$1"
END_DATE="$2"
OUTPUT="$3"

START_TIMESTAMP="$(date --utc +'%Y-%m-%dT%H:%M:%SZ' --date="$START_DATE")"
END_TIMESTAMP="$(date --utc +'%Y-%m-%dT%H:%M:%SZ' --date="$END_DATE")"

ELASTIC='https://train-stats.toolforge.org/backports'

INITIAL_RESPONSE=$(curl -s --fail -XGET "$ELASTIC?start_date=${START_TIMESTAMP}&end_date=${END_TIMESTAMP}&from=0")
FETCHED_COUNT=0
TOTAL_HITS=$(echo "$INITIAL_RESPONSE" | jq -r '.hits.total.value')
printf "Total hits: %s\n" "$TOTAL_HITS"

while [ "$FETCHED_COUNT" -lt "$TOTAL_HITS" ]; do
    RESPONSE=$(curl -s --fail -XGET "$ELASTIC?start_date=${START_TIMESTAMP}&end_date=${END_TIMESTAMP}&from=${FETCHED_COUNT}")
    CURRENT_COUNT=$(echo "$RESPONSE" | jq -r '.hits.hits | length')

    if [ "$CURRENT_COUNT" -eq 0 ]; then
        break
    fi

    FETCHED_COUNT=$((FETCHED_COUNT + CURRENT_COUNT))
    echo "Fetched: $FETCHED_COUNT / $TOTAL_HITS"
    echo "$RESPONSE" | jq -c '.' >> "$OUTPUT"
done
echo "Created $OUTPUT"
