#!/usr/bin/env python
import flask
from flask import Flask, request, jsonify
import requests
import dateparser
from datetime import datetime

app = Flask(__name__)

ELASTIC = 'http://elasticsearch.svc.tools.eqiad1.wikimedia.cloud'
QUERY_FMT = {
    "size": 100,
    "_source": ["@timestamp", "message", "nick"],
    "query": {
        "bool": {
            "filter": [{
                "range": {
                    "@timestamp": {
                        "gte": None,
                        "lte": None
                    }
                }
            }, {
                "bool": {
                    "must": [
                        {"match": {"channel": "#wikimedia-operations"}},
                        {"match": {"project": "production"}},
                        {"match": {"message": {"query": "Synchronized Finished"}}},
                        {"match": {"message": {"query": "(duration"}}}
                    ]
                }
            }]
        }
    },
    "sort": [{"@timestamp": "asc"}]
}


@app.route('/backports')
def backports():
    """
    Require a start date and end date and use those to gather a list of
    backports from elastic search
    format start_date and end_date as YYY-MM-DDTHH:MM:SSZ
    """
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    from_record = request.args.get('from', 0)
    if not start_date or not end_date:
        return jsonify({'error': 'start_date and end_date are required'})
    date_fmt = '%Y-%m-%dT%H:%M:%SZ%z'
    start_date = dateparser.parse(start_date)
    end_date = dateparser.parse(end_date)
    if start_date is None or end_date is None:
        return jsonify({'error': 'start_date and end_date must be valid dates'})
    if start_date > end_date:
        return jsonify({'error': 'start_date must be before end_date'})
    query = QUERY_FMT.copy()
    query['from'] = from_record
    query['query']['bool']['filter'][0]['range']['@timestamp']['gte'] = start_date.strftime(date_fmt)
    query['query']['bool']['filter'][0]['range']['@timestamp']['lte'] = end_date.strftime(date_fmt)
    r = requests.get(
        ELASTIC + '/sal/_search',
        json=query
    )
    r.raise_for_status()
    try:
        results = r.json()
    except requests.exceptions.JSONDecodeError:
        return jsonify({'error': 'Unable to decode JSON'})
    except requests.exceptions.RequestException as e:
        return jsonify({'error': str(e)})
    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=False)
