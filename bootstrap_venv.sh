#!/bin/bash

set -euo pipefail

rm -rf pyvenv
python3 -m venv www/python/venv
source www/python/venv/bin/activate

pip install -U pip wheel
pip install -r src/requirements.txt
