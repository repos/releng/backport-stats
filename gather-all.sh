#!/usr/bin/env bash

for i in {2014..2024}; do
    last_year=$((i-1))

    start_date="$(date --utc +'%Y-%m-%dT%H:%M:%SZ' --date="${last_year}-01-01")"
    end_date="$(date --utc +'%Y-%m-%dT%H:%M:%SZ' --date="$i-01-01")"
    bash gather.sh "$start_date" "$end_date" "data/deploys-$start_date-$end_date.json"
    python3 parse-deploys.py "data/deploys-$start_date-$end_date.json" "data/deploys-$start_date-$end_date.db"
done
bash merge-dbs.sh data/deploys-*.db data/deploys.db
